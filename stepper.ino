#include <Stepper.h>
#include <Arduino.h>
#include "A4988.h"


// using a 200-step motor (most common)
#define MOTOR_STEPS 200
// configure the pins connected
#define DIR 8
#define STEP 9
#define MS1 10
#define MS2 11
#define MS3 12
A4988 stepper(MOTOR_STEPS, DIR, STEP, MS1, MS2, MS3);
const int BUTTON_PIN = 7; // the number of the pushbutton pin
const int LED_PIN =  3;   // the number of the LED pin
int buttonState = 0;

void setup() {
   
    Serial.begin(9600);
    pinMode(LED_PIN, OUTPUT);
    pinMode(BUTTON_PIN, INPUT_PULLUP);
     // Set target motor RPM to 1RPM and microstepping to 1 (full step mode)
    stepper.begin(5, 1);
}

void jogForward () {
  //buttonState = digitalRead(BUTTON_PIN);
  Serial.println("Moving forward 1");
  stepper.move(360);
  //stepper.rotate(8);
}

void stop() {
  stepper.move(0);
}
void jogBackwards (){
  Serial.println("Moving backwards");
  stepper.move(-1);
  //stepper.rotate(-1.6);
}
void loop() {
    // Tell motor to rotate 360 degrees. That's it.
    //stepper.rotate(360);
     // read the state of the pushbutton value:
  buttonState = digitalRead(BUTTON_PIN);
//    if (buttonState == HIGH){
//      stepper.rotate(360);
//      Serial.print("Rotating");
//    } 
//    if (buttonState == LOW){
//      Serial.print("Stop");
//      //stepper.stop();
//      //stepper.disable();
//        //stepper.move(5);
//        stepper.stop();
//      } else {
//        stepper.move(30);
//      }
      if (buttonState == LOW) {
        digitalWrite(LED_PIN, HIGH);
        jogForward();
      } else {
        digitalWrite(LED_PIN, LOW);
        //jogBackwards();
        stop();
      }
     
    
    }
